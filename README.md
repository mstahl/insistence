 ```text
 __             __       __                          
|__.-----.-----|__.-----|  |_.-----.-----.----.-----.
|  |     |__ --|  |__ --|   _|  -__|     |  __|  -__|
|__|__|__|_____|__|_____|____|_____|__|__|____|_____|
```

## Wait, what?

Insistence is a volatile data store that optimizes queries based on their
frequency. Query for a key more often than other keys and it will come back
faster, and over time the data structure will balance itself according to key
hit frequencies.

## How does it work?

The core of Insistence is a data structure like a [binary search
tree](https://en.wikipedia.org/wiki/Binary_search_tree), which balances itself
according to the hit rates of its nodes. When each key is accessed, its hit
rate is incremented, and the tree is rebalanced so that the root node of any
subtree is the node of that subtree with the highest hit rate. Whenever a
node's hit rate increases, it has a chance to move higher up in the tree,
closer to the root node, where it will be found faster in searches.

## Installation

Install with `go get`:

```bash
go get -u github.com/mstahl/insistence
```

## Usage

As a server:

```bash
insistence -d [-p PORT]
```

As a client:

```bash
insistence URL
```

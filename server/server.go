package server

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"net"
	"os"
	"os/signal"

	"gitlab.com/mstahl/insistence/commands"
	"gitlab.com/mstahl/insistence/persistence"
)

// dataStore is a data structure that acts like a map, but optimizes itself
// according to how frequently its keys are accessed. There is only one.
var dataStore *persistence.Tree

func init() {
	dataStore = &persistence.Tree{}
}

/**
 * Listen accepts a port number and spins up an Insistence server listening on
 * that port.
 */
func Listen(port int) {
	fmt.Printf("Listening on port %d\n", port)

	listener, err := net.Listen("tcp", fmt.Sprintf(":%d", port))
	if err != nil {
		log.Fatal(err)
	}
	defer listener.Close()

	handleInterrupt()

	for {
		conn, err := listener.Accept()
		if err != nil {
			log.Fatal(err)
		}

		go handleConnection(conn)
	}
}

func handleConnection(c net.Conn) {
	defer c.Close()
	log.Printf("Accepting connection from %s\n", c.RemoteAddr().String())

	for {
		var parsedCmd commands.InsistenceCmd
		unparsedCmd, err := bufio.NewReader(c).ReadString('\n')
		if err == io.EOF {
			break
		} else if err != nil {
			c.Write([]byte(fmt.Sprintf("Error reading command: '%s'\n", err)))
			continue
		}

		parsedCmd, err = commands.ParseCommand(unparsedCmd)
		if err != nil {
			c.Write([]byte(fmt.Sprintf("Error reading command: '%s'\n", err)))
			continue
		}

		// If the opcode is "quit", close the connection
		if parsedCmd.Opcode == "quit" {
			c.Write([]byte("Bye!\n"))
			break
		}

		cmdResult, err := dataStore.ExecuteCommand(parsedCmd)
		if err != nil {
			c.Write([]byte(fmt.Sprintf("Error executing command: '%s'\n", err)))
			continue
		}

		c.Write([]byte(cmdResult + "\n"))
	}
}

/**
 * handleInterrupt waits for a Ctrl-C or SIGKILL signal from the operating
 * system and gracefully shuts down the server.
 */
func handleInterrupt() {
	// Set up an interrupt handler to catch Ctrl-C and shut down gracefully
	quitChannel := make(chan os.Signal, 1)
	signal.Notify(quitChannel, os.Interrupt)
	go func(q chan os.Signal) {
		_ = <-q
		fmt.Println()
		fmt.Println("Quittin' time. Bye!")
		os.Exit(0)
	}(quitChannel)
}

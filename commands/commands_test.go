package commands_test

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/mstahl/insistence/commands"
)

func TestParseCommandSuccess(t *testing.T) {
	testData := []struct {
		unparsed         string
		expectedOpcode   string
		expectedOperands []string
	}{
		{"set foo bar", "set", []string{"foo", "bar"}},
		{"get foo", "get", []string{"foo"}},
	}

	for _, td := range testData {
		parsed, _ := commands.ParseCommand(td.unparsed)
		assert.Equal(t, td.expectedOpcode, string(parsed.Opcode))
		assert.Equal(t, td.expectedOperands, parsed.Operands)
	}
}

func TestParseCommandFailure(t *testing.T) {
	testData := []struct {
		unparsed    string
		expectedErr string
	}{
		{"foo bar baz", "I don't know how to `foo bar baz` #sorrynotsorry"},
		{"", "I don't know how to `` #sorrynotsorry"},
	}

	for _, td := range testData {
		_, err := commands.ParseCommand(td.unparsed)
		assert.Equal(t, td.expectedErr, fmt.Sprintf("%s", err))
	}
}

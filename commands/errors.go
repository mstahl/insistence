package commands

import (
	"fmt"
	"strings"
)

type ParseError struct {
	UnparsedCommand string
}

func (pe ParseError) Error() string {
	return fmt.Sprintf(
		"I don't know how to `%s` #sorrynotsorry",
		strings.TrimSpace(pe.UnparsedCommand),
	)
}

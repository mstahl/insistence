package commands

import (
	"regexp"
	"strings"
)

type InsistenceOpcode string
type InsistenceCmd struct {
	Opcode   InsistenceOpcode
	Operands []string
}

var splitRegex = regexp.MustCompile("\\s+")
var allCmds = [...]string{
	"get",
	"set",
	"quit",
}

func ParseCommand(unparsed string) (InsistenceCmd, error) {
	cmdPieces := splitRegex.Split(unparsed, -1)

	opcode := strings.ToLower(cmdPieces[0])
	operands := cmdPieces[1:]

	if isValidOpcode(opcode) {
		return InsistenceCmd{
			Opcode:   InsistenceOpcode(opcode),
			Operands: operands,
		}, nil
	} else {
		return InsistenceCmd{}, ParseError{UnparsedCommand: unparsed}
	}
}

func isValidOpcode(opcode string) bool {
	for _, c := range allCmds {
		if c == opcode {
			return true
		}
	}
	return false
}

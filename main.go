package main

import (
	"flag"
	"fmt"
	"os"

	"gitlab.com/mstahl/insistence/server"
)

const defaultPort = 5076
const banner = `
 __             __       __                          
|__.-----.-----|__.-----|  |_.-----.-----.----.-----.
|  |     |__ --|  |__ --|   _|  -__|     |  __|  -__|
|__|__|__|_____|__|_____|____|_____|__|__|____|_____|
`

var serverMode bool
var port int

func init() {
	flag.BoolVar(&serverMode, "server", true, "Starts an insistence server")
	flag.BoolVar(&serverMode, "s", true, "Starts an insistence server (shorthand)")
	flag.IntVar(&port, "port", defaultPort, fmt.Sprintf("Port number to bind to, default %d", defaultPort))
	flag.IntVar(&port, "p", defaultPort, fmt.Sprintf("Port number to bind to, default %d (shorthand)", defaultPort))
}

func main() {
	fmt.Println(banner)

	flag.Parse()

	switch {
	case len(os.Args) == 1 || os.Args[1] == "server":
		server.Listen(port)
	case os.Args[1] == "client":
		fmt.Println("Client mode not yet implemented")
	default:
	}
}

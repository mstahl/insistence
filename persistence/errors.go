package persistence

type ExecutionError struct {
	message string
}

func (ee ExecutionError) Error() string {
	return ee.message
}

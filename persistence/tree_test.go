package persistence_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/mstahl/insistence/persistence"
)

func TestAdd(t *testing.T) {
	tree := persistence.Tree{}

	// First insert replaces blank contents
	tree.Add("3", "foo")
	assert.Equal(t, "3", tree.Key)
	assert.Equal(t, "foo", tree.Value)

	// Inserting 2 adds a left node
	tree.Add("1", "bar")
	assert.NotNil(t, tree.Left)
	assert.Equal(t, "1", tree.Left.Key)
	assert.Equal(t, "bar", tree.Left.Value)

	// Inserting 3 adds a right node
	tree.Add("5", "baz")
	assert.NotNil(t, tree.Right)
	assert.Equal(t, "5", tree.Right.Key)
	assert.Equal(t, "baz", tree.Right.Value)

	// Inserting 4 adds a right node to 3's left node
	tree.Add("2", "quux")
	assert.NotNil(t, tree.Left.Right)
	assert.Equal(t, "2", tree.Left.Right.Key)
	assert.Equal(t, "quux", tree.Left.Right.Value)
}

func TestLookup(t *testing.T) {
	tree := persistence.Tree{}
	tree.Add("3", "foo")
	tree.Add("1", "bar")
	tree.Add("5", "baz")
	tree.Add("2", "quux")

	testData := []struct {
		key   string
		value string
		found bool
	}{
		{"1", "bar", true},
		{"2", "quux", true},
		{"3", "foo", true},
		{"4", "", false},
		{"5", "baz", true},
	}

	for _, td := range testData {
		value, found := tree.Lookup(td.key)
		assert.Equal(t, td.value, value)
		assert.Equal(t, td.found, found)
	}
}

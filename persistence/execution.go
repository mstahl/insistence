package persistence

import (
	"fmt"

	"gitlab.com/mstahl/insistence/commands"
)

/**
 * ExecuteCommand accepts an Insistence command and executes it in a threadsafe
 * manner on the tree.
 */
func (tree *Tree) ExecuteCommand(cmd commands.InsistenceCmd) (string, error) {
	switch cmd.Opcode {
	case "get":
		return tree.doGet(cmd.Operands)
	case "set":
		return "", tree.doSet(cmd.Operands)
	default:
		return "", ExecutionError{fmt.Sprintf("Invalid opcode `%s`", cmd.Opcode)}
	}
}

func (tree *Tree) doGet(args []string) (string, error) {
	// Validate args, return error if invalid
	if len(args) != 1 {
		return "", ExecutionError{fmt.Sprintf("GET accepts 1 argument, got %d", len(args))}
	}

	key := args[0]
	value, found := tree.Lookup(key)
	if found {
		return value, nil
	} else {
		return "", ExecutionError{fmt.Sprintf("Not found: `%s`", key)}
	}
}

func (tree *Tree) doSet(args []string) error {
	// Validate args, return error if invalid
	if len(args) != 2 {
		return ExecutionError{fmt.Sprintf("SET accepts 2 arguments, got %d", len(args))}
	}

	key, value := args[0], args[1]
	tree.Add(key, value)

	return nil
}

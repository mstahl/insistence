package persistence

import "sync"

type Tree struct {
	Left     *Tree
	Right    *Tree
	Key      string
	Value    string
	HitCount int
	mux      sync.Mutex
}

func (tree *Tree) Add(key, value string) {
	// Lock the tree, absolutely necessary for adding keys
	tree.mux.Lock()
	defer tree.mux.Unlock()

	switch {
	case tree.Key == "":
		tree.Key = key
		tree.Value = value
	case key < tree.Key:
		if tree.Left != nil {
			tree.Left.Add(key, value)
		} else {
			tree.Left = &Tree{
				Key:   key,
				Value: value,
			}
		}
	case key > tree.Key:
		if tree.Right != nil {
			tree.Right.Add(key, value)
		} else {
			tree.Right = &Tree{
				Key:   key,
				Value: value,
			}
		}
	default:
		tree.HitCount += 1
		tree.Value = value
	}
}

func (tree *Tree) Lookup(key string) (string, bool) {
	// Lock the tree -- this might not be necessary for lookups...
	tree.mux.Lock()
	defer tree.mux.Unlock()

	switch {
	case tree.Key == "":
		return "", false
	case key < tree.Key:
		if tree.Left != nil {
			return tree.Left.Lookup(key)
		} else {
			return "", false
		}
	case key > tree.Key:
		if tree.Right != nil {
			return tree.Right.Lookup(key)
		} else {
			return "", false
		}
	default:
		return tree.Value, true
	}
}
